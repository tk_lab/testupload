package com.tirks.jagacy.controller;

import com.tirks.jagacy.model.TirksRequest;
import com.tirks.jagacy.service.TirksService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/tirks")
public class JagacyController {

    @Autowired
    private TirksService tirksService;

    @GetMapping("/data")
    public void getData() {
        String request = "TIRKS_NY_ENV";
        tirksService.getAllTirksProps(request);
        System.out.println(tirksService.getAllTirksProps(request));
    }

    public void display() {
        System.out.println("Display method added directly to main");
    }
    public void display2() {
        System.out.println("Display method added directly to main --  need to change");
    }

    public void display3_from_main() {
        System.out.println("Changing this at 11:01 PM - from main ");
    }

    public void display_From_BranchA() {
        System.out.println("Branch A Changes");
    }

    public void testthismethod() {
        System.out.println("changed at 2:57 - branch a changes");
    }

    public void branchC() {
        // Added the method - branchc -- 2:39 -- need to push from branch B to branch b gitlab(remote)
    }
}
