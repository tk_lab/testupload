package com.tirks.jagacy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JagacyApplication {

	public static void main(String[] args) {
		SpringApplication.run(JagacyApplication.class, args);
	}

}
